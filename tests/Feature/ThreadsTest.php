<?php

namespace Tests\Feature;

use App\Reply;
use App\Thread;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ThreadsTest extends TestCase
{
    use RefreshDatabase;
  
    private $thread;

    public function setUp(): void
    {
        parent::setUp();
        $this->thread = factory(Thread::class)->create();
    }

    /**
     * @test
     * */
    public function a_user_can_browse_thread()
    {
        $this->get('/threads')->assertStatus(200)->assertSee($this->thread->title);
    }
    /**
     * @test
     */
    public function a_user_can_see_a_thread()
    {
        $this->get('/threads/' . $this->thread->id)->assertSee($this->thread->title);
    }

    /** @test */
    public function a_user_can_read_replies_that_are_associated_to_a_thread()
    {
        $reply = factory(Reply::class)->create(['thread_id' => $this->thread->id]);
        $this->get('/threads/' . $this->thread->id)->assertSee($reply->body);
    }
}
